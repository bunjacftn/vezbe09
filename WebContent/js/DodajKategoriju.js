$(document).ready(function () {
    var pasusUpozorenje = $('p.upozorenje');
    pasusUpozorenje.hide();

    $('form').submit(function () {
        var naziv = $("input[name=naziv]").val();   

        if (naziv == "") {
            pasusUpozorenje.text("Niste popunili sva polja");
            pasusUpozorenje.slideDown();
            return false;
        }        
        if (naziv.match("^[a-zA-Z0-9]+$") == null) {
            pasusUpozorenje.text("Korisnicko ime sme da sadrzi samo alfanumericke karaktere");
            pasusUpozorenje.show();
            return false;
        }
        return true;
    });

    $('input[type=text]').focus(function () {
        pasusUpozorenje.slideUp();
        $(this).removeClass("nevalidan");
    });

    $('input[type=text]').blur(function () {
        var vrednost = $(this).val();
        if (vrednost == '') {
            $(this).addClass("nevalidan");
        }
    });
});