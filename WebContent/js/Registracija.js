$(document).ready(function () {
    var pasusUpozorenje = $('p.upozorenje');
    pasusUpozorenje.hide();

    $('form').submit(function () {
        var korisnickoIme = $("input[name=korisnickoIme]").val();
        var lozinka = $("input[name=lozinka]").val();
        var ponovljenaLozinka = $("input[name=ponovljenaLozinka]").val();
        console.log(korisnickoIme);
        console.log(lozinka);
        console.log(ponovljenaLozinka);

        if (korisnickoIme == "" || lozinka == "" || ponovljenaLozinka == "") {
            //alert("Niste popunili sva polja");
            pasusUpozorenje.text("Niste popunili sva polja");
            pasusUpozorenje.slideDown();
            return false;
        }
        if (lozinka != ponovljenaLozinka) {
            pasusUpozorenje.text("Lozinke se ne podudaraju!");
            pasusUpozorenje.show();
            return false;
        }
        if (korisnickoIme.match("^[a-zA-Z0-9]+$") == null) {
            pasusUpozorenje.text("Korisnicko ime sme da sadrzi samo alfanumericke karaktere");
            pasusUpozorenje.show();
            return false;
        }
        return true;
    });

    $('input[type=text], input[type=password]').focus(function () {
        pasusUpozorenje.slideUp();
        $(this).removeClass("nevalidan");
    });

    $('input[type=text], input[type=password]').blur(function () {
        var vrednost = $(this).val();
        if (vrednost == '') {
            $(this).addClass("nevalidan");
        }
    });
});