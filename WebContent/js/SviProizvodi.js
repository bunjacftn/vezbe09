$(document).ready(function () { 
    $('form').submit(function () {
        $(this).komada

        var naziv = $("input[name=naziv]").val();

        if (naziv == "") {
            pasusUpozorenje.text("Niste popunili sva polja");
            pasusUpozorenje.slideDown();
            return false;
        }
        if (naziv.match("^[a-zA-Z0-9]+$") == null) {
            pasusUpozorenje.text("Korisnicko ime sme da sadrzi samo alfanumericke karaktere");
            pasusUpozorenje.show();
            return false;
        }
        return true;
    });
})

function promeniTabelu() {
    var forme = document.forms;
    for (i = 0; i < forme.length; i++) {
        var polje = forme[i].komada;
        polje.readOnly = true;

        var btn2 = document.createElement("input");
        btn2.setAttribute('type', 'button');
        btn2.setAttribute("value", "+");
        btn2.setAttribute("onclick", "promeniVrednost(this.parentNode.komada, 1)");
        btn2.style.background = "green";
        forme[i].insertBefore(btn2, polje.nextSibling.nextSibling);


        var btn = document.createElement("input");
        btn.setAttribute('type', 'button');
        btn.setAttribute("value", "-");
        btn.setAttribute("onclick", "promeniVrednost(this.parentNode.komada, -1)");
        btn.style.background = "red";
        forme[i].insertBefore(btn, polje.nextSibling.nextSibling);

    }
}